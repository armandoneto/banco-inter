How to run:
1) Run "mvn clean install" to fetch the dependencies and build the mapstruct implementation
2) Run "mvn spring-boot:run"
3) Swagger endpoint: http://localhost:8080/swagger-ui.html
4) H2 endpoint: http://localhost:8080/h2-console
	4.1) In the H2 connection use the following JDBC URL: jdbc:h2:mem:test

How to run tests:
1) For unit test run "mvn test"
2) For integration tests import the environment "Unique Number API" into postman then import the postman_collection and run it