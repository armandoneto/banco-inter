package br.com.bancointer.digitounico.dao;

import br.com.bancointer.digitounico.model.UniqueNumber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UniqueNumberRepository extends JpaRepository<UniqueNumber, UUID> {

    @Query(value = "SELECT DISTINCT uniqueNumbers FROM UniqueNumber uniqueNumbers" +
            " LEFT OUTER JOIN uniqueNumbers.myUser users" +
            " WHERE users.id =:id")
    Page<UniqueNumber> findAllFromUser(@Param("id") UUID id, Pageable pageable);

}
