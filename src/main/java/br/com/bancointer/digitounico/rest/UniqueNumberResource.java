package br.com.bancointer.digitounico.rest;

import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberInputDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

public interface UniqueNumberResource {

    @GetMapping("/unique-numbers")
    ResponseEntity<List<UniqueNumberDTO>> getAllUniqueNumbers(Pageable pageable);

    @PostMapping("/unique-numbers")
    ResponseEntity<UniqueNumberDTO> saveUniqueNumber(@RequestParam(required = false) UUID user,
                                                     @RequestBody @NotNull @Valid UniqueNumberInputDTO number) throws URISyntaxException;
}
