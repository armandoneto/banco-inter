package br.com.bancointer.digitounico.rest.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler {

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Error methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    private Error processFieldErrors(List<FieldError> fieldErrors) {
        Error error = new Error(BAD_REQUEST.value(), "Validation error");
        for (FieldError fieldError : fieldErrors) {
            error.addSanitizedFieldError(fieldError.getDefaultMessage(), fieldError.getRejectedValue().toString());
        }
        return error;
    }

    private static class Error implements Serializable {
        private final int status;
        private final String message;
        @JsonProperty("errors")
        private List<SanitizedFieldError> sanitizedFieldErrors = new ArrayList<>();

        Error(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public void addSanitizedFieldError(String message, String value) {
            SanitizedFieldError error = new SanitizedFieldError(message, value);
            sanitizedFieldErrors.add(error);
        }

        public List<SanitizedFieldError> getSanitizedFieldError() {
            return sanitizedFieldErrors;
        }
    }

    private static class SanitizedFieldError {
        private String myMessage;
        private String myValue;

        public SanitizedFieldError(String message, String value) {
            myMessage = message;
            myValue = value;
        }

        public String getValue() {
            return myValue;
        }

        public String getMessage() {
            return myMessage;
        }
    }
}