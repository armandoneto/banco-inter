package br.com.bancointer.digitounico.rest.impl;

import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.UserResource;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.rest.exception.BadKeyException;
import br.com.bancointer.digitounico.rest.exception.ResourceNotFoundException;
import br.com.bancointer.digitounico.rest.util.ApiPageable;
import br.com.bancointer.digitounico.rest.util.HeaderUtil;
import br.com.bancointer.digitounico.rest.util.PaginationUtil;
import br.com.bancointer.digitounico.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api/v1")
@Api("User")
public class UserResourceImpl implements UserResource {

    @Autowired
    private UserService myUserService;

    @Override
    @ApiOperation(value = "View a list of users", response = List.class)
    @ApiPageable
    public ResponseEntity<List<UserDTO>> getAllUsers(@ApiIgnore Pageable pageable) {
        Page<UserDTO> page = myUserService.getAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/v1/users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "View a specific user", response = UserDTO.class)
    public ResponseEntity<UserDTO> findUserById(UUID id) throws ResourceNotFoundException {
        Optional<UserDTO> possibleUser = myUserService.findById(id);

        if (!possibleUser.isPresent()) {
            throw new ResourceNotFoundException("Requested user not found");
        }

        return new ResponseEntity<>(possibleUser.get(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Save a new user", response = UserDTO.class)
    public ResponseEntity<UserDTO> saveUser(UserDTO userDTO) throws URISyntaxException {
        userDTO = myUserService.save(userDTO);

        return ResponseEntity.created(new URI("/api/v1/users/" + userDTO.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, userDTO.getId().toString()))
                .body(userDTO);
    }

    @Override
    @ApiOperation(value = "Update an existent user", response = UserDTO.class)
    public ResponseEntity<UserDTO> updateUser(UUID id, UserDTO userDTO) throws ResourceNotFoundException {
        Optional<UserDTO> possibleUser = myUserService.findById(id);

        if (!possibleUser.isPresent()) {
            throw new ResourceNotFoundException("Requested user not found");
        }

        userDTO.setId(possibleUser.get().getId());
        userDTO = myUserService.save(userDTO);

        return ResponseEntity.ok(userDTO);
    }

    @Override
    @ApiOperation(value = "Delete an existent user")
    public ResponseEntity deleteUserById(UUID id) throws ResourceNotFoundException {
        Optional<UserDTO> possibleUser = myUserService.findById(id);

        if (!possibleUser.isPresent()) {
            throw new ResourceNotFoundException("Requested user not found");
        }

        myUserService.delete(id);

        return ResponseEntity.accepted().body("User deleted");
    }

    @Override
    @ApiOperation(value = "Add public key to an existent user", response = UserDTO.class)
    public ResponseEntity<UserDTO> addPublicKeyToUser(UUID id, String publicKey) throws BadKeyException, ResourceNotFoundException {
        Optional<UserDTO> possibleUser = myUserService.findById(id);

        if (!possibleUser.isPresent()) {
            throw new ResourceNotFoundException("Requested user not found");
        }

        UserDTO userDTO = possibleUser.get();
        userDTO.setPublicKey(publicKey);
        UserDTO returnDTO = userDTO.clone();

        try {
            returnDTO = myUserService.encryptDataIfPublicKeyIsPresent(returnDTO);
            myUserService.save(userDTO);
        } catch(InvalidKeyException | IOException | InvalidCipherTextException ex) {
            throw new BadKeyException("User public key is not valid");
        }

        return ResponseEntity.ok(returnDTO);
    }
}
