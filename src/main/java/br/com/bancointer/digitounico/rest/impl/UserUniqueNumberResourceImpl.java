package br.com.bancointer.digitounico.rest.impl;

import br.com.bancointer.digitounico.rest.UserResource;
import br.com.bancointer.digitounico.rest.UserUniqueNumberResource;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.rest.exception.ResourceNotFoundException;
import br.com.bancointer.digitounico.rest.util.ApiPageable;
import br.com.bancointer.digitounico.rest.util.HeaderUtil;
import br.com.bancointer.digitounico.rest.util.PaginationUtil;
import br.com.bancointer.digitounico.service.UniqueNumberService;
import br.com.bancointer.digitounico.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api/v1")
@Api("User")
public class UserUniqueNumberResourceImpl implements UserUniqueNumberResource {

    @Autowired
    private UniqueNumberService myUniqueNumberService;

    @Autowired
    private UserService myUserService;

    @Override
    @ApiOperation(value = "View a list of unique numbers from an user", response = List.class)
    @ApiPageable
    public ResponseEntity<List<UniqueNumberDTO>> getAllUniqueNumbersFromUser(@PathVariable("id") UUID id, @ApiIgnore Pageable pageable) {
        Optional<UserDTO> possibleUser = myUserService.findById(id);

        Page<UniqueNumberDTO> page = myUniqueNumberService.getAllFromUser(possibleUser.get(), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/v1/users/" + id + "/unique-numbers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
