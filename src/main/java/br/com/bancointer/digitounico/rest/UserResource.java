package br.com.bancointer.digitounico.rest;

import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.rest.exception.BadKeyException;
import br.com.bancointer.digitounico.rest.exception.ResourceNotFoundException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

public interface UserResource {

    @GetMapping("/users")
    ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable);

    @GetMapping("/users/{id}")
    ResponseEntity<UserDTO> findUserById(@PathVariable UUID id) throws ResourceNotFoundException;

    @PostMapping("/users")
    ResponseEntity<UserDTO> saveUser(@RequestBody @Valid UserDTO userDTO) throws URISyntaxException;

    @PutMapping("/users/{id}")
    ResponseEntity<UserDTO> updateUser(@PathVariable UUID id, @RequestBody @Valid UserDTO userDTO) throws URISyntaxException, ResourceNotFoundException;

    @DeleteMapping("/users/{id}")
    ResponseEntity deleteUserById(@PathVariable UUID id) throws ResourceNotFoundException;

    @PutMapping("/users/{id}/public-key")
    ResponseEntity<UserDTO> addPublicKeyToUser(@PathVariable UUID id, @RequestBody String publicKey) throws BadKeyException, ResourceNotFoundException;

}
