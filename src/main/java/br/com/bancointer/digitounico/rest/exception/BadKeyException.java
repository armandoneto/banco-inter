package br.com.bancointer.digitounico.rest.exception;

public class BadKeyException extends Exception {

    public BadKeyException() {
        super();
    }

    public BadKeyException(String message) {
        super(message);
    }

}
