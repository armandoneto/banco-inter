package br.com.bancointer.digitounico.rest.error;

import br.com.bancointer.digitounico.rest.exception.BadKeyException;
import br.com.bancointer.digitounico.rest.exception.ResourceNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class BadKeyExceptionHandler {

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(BadKeyException.class)
    public Error badKeyException(ResourceNotFoundException ex) {
        return new Error(BAD_REQUEST.value(), ex.getMessage());
    }

    private static class Error implements Serializable {
        private final int status;
        private final String message;

        Error(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }
    }
}