package br.com.bancointer.digitounico.rest.impl;

import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.rest.UniqueNumberResource;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberInputDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.rest.util.HeaderUtil;
import br.com.bancointer.digitounico.rest.util.PaginationUtil;
import br.com.bancointer.digitounico.service.UniqueNumberService;
import br.com.bancointer.digitounico.service.UserService;
import br.com.bancointer.digitounico.service.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api/v1")
@Api("Unique Number")
public class UniqueNumberResourceImpl implements UniqueNumberResource {

    @Autowired
    private UniqueNumberService myUniqueNumberService;

    @Autowired
    private UserService myUserService;

    @Autowired
    private UserMapper myUserMapper;

    @Override
    @ApiOperation(value = "View a list of unique number", response = List.class)
    public ResponseEntity<List<UniqueNumberDTO>> getAllUniqueNumbers(Pageable pageable) {
        Page<UniqueNumberDTO> page = myUniqueNumberService.getAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/v1/unique-numbers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Compute a new unique number", response = UniqueNumberDTO.class)
    public ResponseEntity<UniqueNumberDTO> saveUniqueNumber(UUID user, UniqueNumberInputDTO number) throws URISyntaxException {
        Optional<UserDTO> possibleUser;
        if (user != null) {
            possibleUser = myUserService.findById(user);

            if (!possibleUser.isPresent()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user found for the given UUID");
            }
        } else {
            possibleUser = Optional.empty();
        }

        UniqueNumber uniqueNumber = myUniqueNumberService
                .computeUniqueNumber(number.getMyNumber(), number.getMyRepeat())
                .newBuilderFromCurrent()
                .withNumber(number.getMyNumber())
                .build();

        if (!uniqueNumber.getRepeat().equals(number.getMyRepeat())) {
            uniqueNumber = uniqueNumber.newBuilderFromCurrent()
                    .withRepeat(Integer.parseInt(number.getMyRepeat()))
                    .withResult(myUniqueNumberService
                            .computeUniqueNumber(uniqueNumber.getFirstIterationResult(), number.getMyRepeat()))
                    .build();
        }

        UniqueNumberDTO result = myUniqueNumberService.save(setUserIntoUniqueNumber(possibleUser, uniqueNumber));

        return ResponseEntity.created(new URI("/api/v1/unique-numbers/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    private UniqueNumber setUserIntoUniqueNumber(Optional<UserDTO> possibleUser, UniqueNumber uniqueNumber) {
        if (possibleUser.isPresent()) {
            uniqueNumber = uniqueNumber.newBuilderFromCurrent()
                    .withUser(myUserMapper.toEntity(possibleUser.get()))
                    .build();
        }
        return uniqueNumber;
    }
}
