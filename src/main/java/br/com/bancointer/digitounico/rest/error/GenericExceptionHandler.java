package br.com.bancointer.digitounico.rest.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice
public class GenericExceptionHandler {

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Error genericException(Exception ex) {
        return new Error(INTERNAL_SERVER_ERROR.value(),
                "Something went wrong. Please contact the system admin.",
                ex.getMessage());
    }

    private static class Error implements Serializable {
        private final int status;
        private final String message;
        private final String log;

        Error(int status, String message, String log) {
            this.status = status;
            this.message = message;
            this.log = log;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public String getLog() {
            return log;
        }
    }
}