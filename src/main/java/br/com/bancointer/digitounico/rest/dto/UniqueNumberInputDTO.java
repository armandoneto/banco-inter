package br.com.bancointer.digitounico.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UniqueNumberInputDTO implements Serializable {
    @JsonProperty("number")
    @Pattern(message = "Given \"number\" value must be a number and cannot have only zeros",
            regexp = "(?!0+$)(^[0-9]*$)")
    @Size(message = "Value must be greater or equal 1 and lower or equal to 10^1000000",
            min = 1, max = 1000001)
    private String myNumber;
    @JsonProperty("repeat")
    @Pattern(message = "Given \"repeat\" value must be a number and cannot have only zeros",
            regexp = "(?!0+$)(^[0-9]*$)")
    @Size(message = "Value must be greater or equal 1 and lower or equal to 10^5",
            min = 1, max = 6)
    private String myRepeat;

    public String getMyNumber() {
        return myNumber;
    }

    public void setMyNumber(String myNumber) {
        this.myNumber = myNumber;
    }

    public String getMyRepeat() {
        return myRepeat;
    }

    public void setMyRepeat(String myRepeat) {
        this.myRepeat = myRepeat;
    }
}
