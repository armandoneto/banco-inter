package br.com.bancointer.digitounico.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class UniqueNumberDTO implements Serializable {

    @JsonProperty("id")
    private UUID myId;
    @JsonProperty("number")
    private String myNumber;
    @JsonProperty("repeat")
    private Integer myRepeat;
    @JsonProperty("result")
    private Integer myResult;
    @JsonProperty("firstIterationResult")
    private Integer myFirstIterationResult;

    public UUID getId() {
        return myId;
    }

    public void setId(UUID id) {
        myId = id;
    }

    public String getNumber() {
        return myNumber;
    }

    public void setNumber(String number) {
        myNumber = number;
    }

    public Integer getRepeat() {
        return myRepeat;
    }

    public void setRepeat(Integer repeat) {
        myRepeat = repeat;
    }

    public Integer getResult() {
        return myResult;
    }

    public void setResult(Integer result) {
        myResult = result;
    }

    public Integer getFirstIterationResult() {
        return myFirstIterationResult;
    }

    public void setFirstIterationResult(Integer firstIterationResult) {
        myFirstIterationResult = firstIterationResult;
    }
}
