package br.com.bancointer.digitounico.rest;

import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.rest.exception.ResourceNotFoundException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

public interface UserUniqueNumberResource {

    @GetMapping("/users/{id}/unique-numbers")
    ResponseEntity<List<UniqueNumberDTO>> getAllUniqueNumbersFromUser(@PathParam("id") UUID id, Pageable pageable);
}
