package br.com.bancointer.digitounico.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.UUID;

public class UserDTO implements Serializable {

    @JsonProperty("id")
    private UUID myId;
    @JsonProperty("name")
    private String myName;
    @JsonProperty("email")
    @Email(message = "Given value is not a valid email.")
    private String myEmail;
    private String myPublicKey;

    public UserDTO clone() {
        UserDTO clone = new UserDTO();
        clone.setName(myName);
        clone.setEmail(myEmail);
        clone.setId(myId);
        clone.setPublicKey(myPublicKey);

        return clone;
    }

    public UUID getId() {
        return myId;
    }

    public void setId(UUID id) {
        myId = id;
    }

    public String getName() {
        return myName;
    }

    public void setName(String name) {
        myName = name;
    }

    public String getEmail() {
        return myEmail;
    }

    public void setEmail(String email) {
        myEmail = email;
    }

    @JsonIgnore
    public String getPublicKey() {
        return myPublicKey;
    }

    public void setPublicKey(String publicKey) {
        this.myPublicKey = publicKey;
    }
}
