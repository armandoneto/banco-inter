package br.com.bancointer.digitounico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class DigitoUnicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitoUnicoApplication.class, args);
	}

}
