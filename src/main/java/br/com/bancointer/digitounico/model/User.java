package br.com.bancointer.digitounico.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static com.google.common.collect.ImmutableSet.copyOf;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private UUID myId;

    @Column(name = "name")
    @NotNull
    private String myName;

    @Column(name = "email")
    @NotNull
    @Email
    private String myEmail;

    @Column(name = "public_key")
    private String myPublicKey;

    @OneToMany(mappedBy = "myUser", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<UniqueNumber> myUniqueNumbers = new HashSet<>();

    public User() {}

    public User(Builder builder) {
        myId = builder.myId;
        myName = builder.myName;
        myEmail = builder.myEmail;
        myUniqueNumbers = builder.myUniqueNumbers;
        myPublicKey = builder.myPublicKey;
    }

    public UUID getId() {
        return myId;
    }

    public String getName() {
        return myName;
    }

    public String getEmail() {
        return myEmail;
    }

    public String getPublicKey() {
        return myPublicKey;
    }

    public Set<UniqueNumber> getUniqueNumbers() {
        return new HashSet<>(myUniqueNumbers);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Builder newBuilderFromCurrent() {
        return new Builder(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return new EqualsBuilder()
                .append(myId, user.myId)
                .append(myName, user.myName)
                .append(myEmail, user.myEmail)
                .append(myPublicKey, user.myPublicKey)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(myId)
                .append(myName)
                .append(myEmail)
                .append(myPublicKey)
                .build();
    }

    public static class Builder {
        private UUID myId;
        private String myName;
        private String myEmail;
        private String myPublicKey;
        private Set<UniqueNumber> myUniqueNumbers = new HashSet<>();

        private Builder() {
            // Intentionally empty
        }

        private Builder(User original) {
            myId = original.myId;
            myName = original.myName;
            myEmail = original.myEmail;
            myPublicKey = original.myPublicKey;
            myUniqueNumbers = new HashSet<>(original.myUniqueNumbers);
        }

        public Builder withId(UUID id) {
            myId = id;
            return this;
        }

        public Builder withName(String name) {
            myName = name;
            return this;
        }

        public Builder withEmail(String email) {
            myEmail = email;
            return this;
        }

        public Builder withPublicKey(String publicKey) {
            myPublicKey = publicKey;
            return this;
        }

        public Builder withUniqueNumbers(Set<UniqueNumber> uniqueNumbers) {
            myUniqueNumbers = new HashSet<>(uniqueNumbers);
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
