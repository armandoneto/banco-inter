package br.com.bancointer.digitounico.model;

import br.com.bancointer.digitounico.service.cache.Cacheable;
import br.com.bancointer.digitounico.service.cache.annotations.CacheKey;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "unique_numbers")
public class UniqueNumber implements Cacheable<UniqueNumber> {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private UUID myId;

    @Column(name = "number")
    @NotNull
    private String myNumber;

    @Column(name = "repeat")
    @NotNull
    private Integer myRepeat;

    @Column(name = "result")
    @NotNull
    private Integer myResult;

    @Column(name = "first_iteration_result")
    @NotNull
    private Integer myFirstIterationResult;

    @ManyToOne
    @JsonIgnoreProperties("myUniqueNumbers")
    @JoinColumn(name = "user_id", nullable = true)
    private User myUser;

    public UniqueNumber() {
    }

    public UniqueNumber(Builder builder) {
        myId = builder.myId;
        myNumber = builder.myNumber;
        myRepeat = builder.myRepeat;
        myResult = builder.myResult;
        myFirstIterationResult = builder.myFirstIterationResult;
        myUser = builder.myUser;
    }

    public UUID getId() {
        return myId;
    }

    @CacheKey
    public String getNumber() {
        return myNumber;
    }

    public Integer getRepeat() {
        return myRepeat;
    }

    public Integer getResult() {
        return myResult;
    }

    public Integer getFirstIterationResult() {
        return myFirstIterationResult;
    }

    public User getUser() {
        return myUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UniqueNumber uniqueNumber = (UniqueNumber) o;
        return new EqualsBuilder()
                .append(myId, uniqueNumber.myId)
                .append(myNumber, uniqueNumber.myNumber)
                .append(myRepeat, uniqueNumber.myRepeat)
                .append(myResult, uniqueNumber.myResult)
                .append(myFirstIterationResult, uniqueNumber.myFirstIterationResult)
                .append(myUser, uniqueNumber.myUser)
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(myId)
                .append(myNumber)
                .append(myRepeat)
                .append(myResult)
                .append(myFirstIterationResult)
                .append(myUser)
                .build();
    }

    @Override
    public UniqueNumber get() {
        return this;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Builder newBuilderFromCurrent() {
        return new Builder(this);
    }

    public static class Builder {
        private UUID myId;
        private String myNumber;
        private Integer myRepeat;
        private Integer myResult;
        private Integer myFirstIterationResult;
        private User myUser;

        private Builder() {
            // Intentionally empty
        }

        private Builder(UniqueNumber original) {
            myId = original.myId;
            myNumber = original.myNumber;
            myRepeat = original.myRepeat;
            myResult = original.myResult;
            myFirstIterationResult = original.myFirstIterationResult;
            myUser = original.myUser;
        }

        public Builder withId(UUID id) {
            myId = id;
            return this;
        }

        public Builder withNumber(String number) {
            myNumber = number;
            return this;
        }

        public Builder withRepeat(Integer repeat) {
            myRepeat = repeat;
            return this;
        }

        public Builder withResult(Integer result) {
            myResult = result;
            return this;
        }

        public Builder withFirstIterationResult(Integer firstIterationResult) {
            myFirstIterationResult = firstIterationResult;
            return this;
        }

        public Builder withUser(User user) {
            myUser = user;
            return this;
        }

        public UniqueNumber build() {
            return new UniqueNumber(this);
        }
    }
}
