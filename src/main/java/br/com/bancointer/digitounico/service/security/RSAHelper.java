package br.com.bancointer.digitounico.service.security;

import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.util.encoders.Base64;
import sun.security.rsa.RSAPublicKeyImpl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;

import static org.bouncycastle.util.encoders.Hex.decode;
import static org.bouncycastle.util.encoders.Hex.toHexString;

public class RSAHelper {

    private static final String RSA = "RSA";
    private static final int KEY_SIZE = 2048;

    private RSAHelper() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static KeyPair generateKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(RSA);
        keyGen.initialize(KEY_SIZE);
        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    public static PublicKey createPublicKeyFromString(String publicKey) throws InvalidKeyException {
        return new RSAPublicKeyImpl(Base64.decode(publicKey));
    }

    public static String encrypt(PublicKey publicKey, String inputData)
            throws InvalidCipherTextException, IOException {
        AsymmetricKeyParameter asymmetricPublicKey =
                PublicKeyFactory.createKey(publicKey.getEncoded());
        AsymmetricBlockCipher engine = new PKCS1Encoding(new RSAEngine());
        engine.init(true, asymmetricPublicKey);

        byte[] messageBytes = inputData.getBytes();
        byte[] hexEncodedCipher = engine.processBlock(messageBytes, 0, messageBytes.length);

        return toHexString(hexEncodedCipher);
    }

    public static String decrypt(PrivateKey privateKey, String encryptedData)
            throws IOException, InvalidCipherTextException {
        AsymmetricKeyParameter asymmetricPrivateKey =
                PrivateKeyFactory.createKey(privateKey.getEncoded());
        AsymmetricBlockCipher engine = new PKCS1Encoding(new RSAEngine());
        engine.init(false, asymmetricPrivateKey);

        byte[] messageBytes = decode(encryptedData);
        byte[] hexEncodedCipher = engine.processBlock(messageBytes, 0, messageBytes.length);

        return new String(hexEncodedCipher);
    }
}
