package br.com.bancointer.digitounico.service.mapper;

import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UniqueNumberMapper.class})
public interface UserMapper {

    UserDTO toDTO(User user);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "name", target = "withName")
    @Mapping(source = "email", target = "withEmail")
    @Mapping(source = "publicKey", target = "withPublicKey")
    User toEntity(UserDTO userDTO);

    List<UserDTO> toDTO(List<User> users);

    List<User> toEntity(List<UserDTO> userDTOs);
}
