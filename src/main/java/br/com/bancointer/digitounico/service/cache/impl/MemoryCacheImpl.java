package br.com.bancointer.digitounico.service.cache.impl;

import br.com.bancointer.digitounico.service.cache.Cacheable;
import br.com.bancointer.digitounico.service.cache.MemoryCache;
import br.com.bancointer.digitounico.service.cache.annotations.CacheKey;
import br.com.bancointer.digitounico.service.cache.annotations.Cached;
import br.com.bancointer.digitounico.service.cache.exception.CacheKeyNotFoundException;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Arrays.stream;

@Aspect
@Component
public class MemoryCacheImpl extends MemoryCache {

    private MemoryCacheImpl() {
        super();
    }

    @Override
    protected Cacheable get(Class clazz, Object key) {
        Map<Object, Pair<Long, Cacheable>> entityMap = myCache.get(clazz);
        if (entityMap == null) {
            return null;
        }

        Pair<Long, Cacheable> cachedEntity =
                entityMap.compute(key,
                        (mapKey, value) -> (value == null ?
                                null : new MutablePair<>(System.currentTimeMillis(), value.getValue())));
        return cachedEntity == null ? null : entityMap.get(key).getValue();
    }

    @Override
    protected boolean put(Cacheable entity, Object key) {
        Map<Object, Pair<Long, Cacheable>> entityMap = myCache.get(entity.getClass());
        if (entityMap == null) {
            myCache.put(entity.getClass(), new ConcurrentHashMap<>());
            entityMap = myCache.get(entity.getClass());
        }

        removeOldestEntry(entityMap);

        return null == entityMap.put(
                key, new MutablePair<Long, Cacheable>(System.currentTimeMillis(), entity))
                ? false : true;
    }

    @Override
    @Around("@annotation(br.com.bancointer.digitounico.service.cache.annotations.Cached)")
    public Object searchInCache(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();

        Cached cached = method.getAnnotation(Cached.class);
        Class clazz = cached.value();

        Cacheable cacheable = get(clazz, proceedingJoinPoint.getArgs()[0]);

        if (cacheable != null) {
            return cacheable.get();
        }

        cacheable = (Cacheable) proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());

        put(cacheable, findCacheKey(cacheable).invoke(cacheable));

        return cacheable.get();
    }

    public void clearCache() {
        myCache = new ConcurrentHashMap<>();
    }

    private Method findCacheKey(Cacheable cacheable) {
        Optional<Method> possibleMethod = stream(cacheable.get()
                .getClass().getMethods())
                .filter(method -> method.getDeclaredAnnotationsByType(CacheKey.class).length > 0)
                .findFirst();

        if (!possibleMethod.isPresent()) {
            throw new CacheKeyNotFoundException(cacheable.getClass());
        }

        return possibleMethod.get();
    }

    private void removeOldestEntry(Map<Object, Pair<Long, Cacheable>> entityMap) {
        if (entityMap.size() >= cacheMaxSize) {
            Map.Entry<Object, Pair<Long, Cacheable>> minEntry =
                    entityMap.entrySet()
                            .stream()
                            .min((a, b) -> a.getValue().getLeft().compareTo(b.getValue().getLeft()))
                            .get();

            entityMap.remove(minEntry.getKey());
        }
    }
}
