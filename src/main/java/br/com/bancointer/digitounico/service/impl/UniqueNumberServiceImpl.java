package br.com.bancointer.digitounico.service.impl;

import br.com.bancointer.digitounico.dao.UniqueNumberRepository;
import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.service.UniqueNumberService;
import br.com.bancointer.digitounico.service.cache.annotations.Cached;
import br.com.bancointer.digitounico.service.mapper.UniqueNumberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.math.BigInteger.ZERO;

@Service
@Transactional
public class UniqueNumberServiceImpl implements UniqueNumberService {
    private static final BigInteger TEN = new BigInteger("10");

    @Autowired
    private UniqueNumberMapper myUniqueNumberMapper;

    @Autowired
    private UniqueNumberRepository myUniqueNumberRepository;

    @Override
    @Transactional(readOnly = true)
    public Page<UniqueNumberDTO> getAll(Pageable pageable) {
        return myUniqueNumberRepository.findAll(pageable).map(myUniqueNumberMapper::toDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UniqueNumberDTO> getAllFromUser(UserDTO userDTO, Pageable pageable) {
        return myUniqueNumberRepository.findAllFromUser(userDTO.getId(), pageable).map(myUniqueNumberMapper::toDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UniqueNumberDTO> findById(UUID id) {
        Optional<UniqueNumber> possibleUniqueNumber = myUniqueNumberRepository.findById(id);
        if (possibleUniqueNumber.isPresent()) {
            return Optional.of(myUniqueNumberMapper.toDTO(possibleUniqueNumber.get()));
        }
        return Optional.empty();
    }

    @Override
    public UniqueNumberDTO save(UniqueNumber uniqueNumber) {
        return myUniqueNumberMapper.toDTO(myUniqueNumberRepository.save(uniqueNumber));
    }

    @Override
    public boolean delete(UUID id) {
        myUniqueNumberRepository.deleteById(id);
        return !findById(id).isPresent();
    }

    @Override
    @Cached(UniqueNumber.class)
    public UniqueNumber computeUniqueNumber(String number, String runTimes) throws NumberFormatException {
        BigInteger repeat = new BigInteger(runTimes);
        UniqueNumber.Builder builder = UniqueNumber.newBuilder()
                .withNumber(number)
                .withRepeat(repeat.intValue());

        String[] partitionedNumber = number.split("(?<=\\G.{10000})");

        List<BigInteger> computedNumbers = Arrays.asList(partitionedNumber)
                .parallelStream()
                .map(this::computePartitionedNumbers)
                .collect(Collectors.toList());

        BigInteger sum = ZERO;

        for (BigInteger partialResult : computedNumbers) {
            sum = sum.add(partialResult);
        }

        sum = computeUniqueNumber(sum);

        builder.withFirstIterationResult(sum.intValue());

        if (repeat.compareTo(BigInteger.ONE) > 0) {
            sum = computeUniqueNumber(sum.multiply(repeat));
        }

        builder.withResult(sum.intValue());

        return builder.build();
    }

    @Override
    public Integer computeUniqueNumber(Integer number, String runTimes) {
        Integer repeat = Integer.parseInt(runTimes);
        Integer newNumber = number * repeat;

        return computeUniqueNumber(new BigInteger(newNumber.toString())).intValue();
    }

    /**
     * Computes the unique digit of a given partioned number
     * in String format.
     * First create a new {@link BigInteger} from the given
     * {@link String} and then finds the divider and remainder
     * by 10 of the given number, then sum the remainder result and
     * change the original number by its divider.
     * This method is used to find the first iteration of all parts
     * of a given number.
     *
     * @param partitionedNumber a {@link String} representing the
     *                          number to compute. Must be a valid number
     * @return a {@link BigInteger} with the sum of all digits of given
     * partitioned number
     */
    private BigInteger computePartitionedNumbers(String partitionedNumber) {
        BigInteger originalNumber = new BigInteger(partitionedNumber);
        BigInteger[] dividerAndRemainder;
        BigInteger sum = ZERO;

        while (originalNumber.compareTo(ZERO) > 0) {
            dividerAndRemainder = originalNumber.divideAndRemainder(TEN);
            sum = sum.add(dividerAndRemainder[1]);
            originalNumber = dividerAndRemainder[0];
        }

        return sum;
    }

    /**
     * Computes the unique digit of a given originalNumber.
     * First finds the divider and remainder by 10 of the
     * given number, then sum the remainder result and
     * change the original number by its divider. Repeat
     * until original number is lower or equal 0.
     * If the result is 10 or greater, repeat the process.
     *
     * @param originalNumber a {@link BigInteger} representing the
     *                       number to compute
     * @return a {@link BigInteger} with the unique number
     */
    private BigInteger computeUniqueNumber(BigInteger originalNumber) {
        BigInteger[] dividerAndRemainder;
        BigInteger sum = ZERO;

        while (originalNumber.compareTo(ZERO) > 0) {
            dividerAndRemainder = originalNumber.divideAndRemainder(TEN);
            sum = sum.add(dividerAndRemainder[1]);
            originalNumber = dividerAndRemainder[0];
        }

        if (sum.divideAndRemainder(TEN)[0].compareTo(ZERO) > 0) {
            return computeUniqueNumber(sum);
        }

        return sum;
    }
}
