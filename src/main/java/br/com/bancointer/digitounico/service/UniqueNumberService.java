package br.com.bancointer.digitounico.service;

import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface UniqueNumberService {

    Page<UniqueNumberDTO> getAll(Pageable pageable);

    Optional<UniqueNumberDTO> findById(UUID id);

    UniqueNumberDTO save(UniqueNumber uniqueNumber);

    boolean delete(UUID id);

    UniqueNumber computeUniqueNumber(String number, String runTimes);

    Integer computeUniqueNumber(Integer number, String runTimes);

    Page<UniqueNumberDTO> getAllFromUser(UserDTO userDTO, Pageable pageable);
}
