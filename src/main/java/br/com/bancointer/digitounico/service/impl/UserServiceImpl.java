package br.com.bancointer.digitounico.service.impl;

import br.com.bancointer.digitounico.dao.UserRepository;
import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import br.com.bancointer.digitounico.service.UserService;
import br.com.bancointer.digitounico.service.mapper.UserMapper;
import br.com.bancointer.digitounico.service.security.RSAHelper;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository myUserRepository;

    @Autowired
    private UserMapper myUserMapper;

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> getAll(Pageable pageable) {
        return myUserRepository.findAll(pageable).map(myUserMapper::toDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserDTO> findById(UUID id) {
        return myUserRepository.findById(id).map(myUserMapper::toDTO);
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        User user = myUserMapper.toEntity(userDTO);
        return myUserMapper.toDTO(myUserRepository.save(user));
    }

    @Override
    public boolean delete(UUID id) {
        myUserRepository.deleteById(id);
        return !findById(id).isPresent();
    }

    @Override
    public UserDTO encryptDataIfPublicKeyIsPresent(UserDTO userDTO) throws InvalidKeyException, IOException, InvalidCipherTextException {
        if (userDTO.getPublicKey() != null && !"".equals(userDTO.getPublicKey())) {
            PublicKey key = RSAHelper.createPublicKeyFromString(userDTO.getPublicKey());
            userDTO.setName(RSAHelper.encrypt(key, userDTO.getName()));
            userDTO.setEmail(RSAHelper.encrypt(key, userDTO.getEmail()));
        }

        return userDTO;
    }
}
