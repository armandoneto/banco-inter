package br.com.bancointer.digitounico.service;

import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    Page<UserDTO> getAll(Pageable pageable);

    Optional<UserDTO> findById(UUID id);

    UserDTO save(UserDTO user);

    boolean delete(UUID id);

    UserDTO encryptDataIfPublicKeyIsPresent(UserDTO userDTO) throws InvalidKeyException, IOException, InvalidCipherTextException;
}
