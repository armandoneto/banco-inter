package br.com.bancointer.digitounico.service.mapper;

import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.model.User;
import br.com.bancointer.digitounico.rest.dto.UniqueNumberDTO;
import br.com.bancointer.digitounico.rest.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UniqueNumberMapper {

    UniqueNumberDTO toDTO(UniqueNumber uniqueNumber);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "number", target = "withNumber")
    @Mapping(source = "repeat", target = "withRepeat")
    @Mapping(source = "result", target = "withResult")
    @Mapping(source = "firstIterationResult", target = "withFirstIterationResult")
    UniqueNumber toEntity(UniqueNumberDTO uniqueNumberDTO);

    List<UniqueNumberDTO> toDTO(List<UniqueNumber> uniqueNumbers);

    List<UniqueNumber> toEntity(List<UniqueNumberDTO> uniqueNumberDTOs);
}
