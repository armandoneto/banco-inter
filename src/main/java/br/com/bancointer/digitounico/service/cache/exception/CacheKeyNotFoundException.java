package br.com.bancointer.digitounico.service.cache.exception;

public class CacheKeyNotFoundException extends RuntimeException {

    public CacheKeyNotFoundException() {
        super();
    }

    public CacheKeyNotFoundException(Class clazz) {
        super("Class " + clazz.getName() + " must have a method annotated with @CacheKey");
    }
}
