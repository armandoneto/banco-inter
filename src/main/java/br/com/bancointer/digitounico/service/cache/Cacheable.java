package br.com.bancointer.digitounico.service.cache;

public interface Cacheable<T> {

    T get();
}
