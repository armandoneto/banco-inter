package br.com.bancointer.digitounico.service.cache;

import org.apache.commons.lang3.tuple.Pair;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class MemoryCache {

    protected static int cacheMaxSize = 10;
    protected Map<Class, Map<Object, Pair<Long, Cacheable>>> myCache;

    protected MemoryCache() {
        myCache = new ConcurrentHashMap<>();
    }

    protected abstract Cacheable get(Class clazz, Object key);

    protected abstract boolean put(Cacheable entity, Object key);

    public abstract Object searchInCache(ProceedingJoinPoint proceedingJoinPoint) throws Throwable;

    public abstract void clearCache();
}
