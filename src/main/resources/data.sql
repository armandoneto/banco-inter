DROP TABLE IF EXISTS users, unique_numbers;

CREATE TABLE users (
  id UUID default random_uuid(),
  name VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL,
  public_key VARCHAR(250),
  PRIMARY KEY (id)
);

CREATE TABLE unique_numbers (
  id UUID default random_uuid() PRIMARY KEY,
  number TEXT NOT NULL,
  repeat LONG NOT NULL,
  result int NOT NULL,
  first_iteration_result LONG NOT NULL,
  user_id UUID,
  PRIMARY KEY (id),
  CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO users (name, email) VALUES
  ('One', 'one@mail.com'),
  ('Two', 'two@mail.com'),
  ('Three', 'three@mail.com');