package br.com.bancointer.digitounico.service.security;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRSAHelper {
    private String myData;
    private KeyPair myKeyPair;

    @BeforeEach
    public void setup() throws NoSuchAlgorithmException {
        myData = "Super secure message";
        myKeyPair = RSAHelper.generateKey();
    }

    @Test
    public void shouldEncryptAndDecrypt() throws IOException, InvalidCipherTextException {
        String myEncryptedData = RSAHelper.encrypt(myKeyPair.getPublic(), myData);
        String myDecryptedData = RSAHelper.decrypt(myKeyPair.getPrivate(), myEncryptedData);

        assertEquals(myData, myDecryptedData);
    }
}
