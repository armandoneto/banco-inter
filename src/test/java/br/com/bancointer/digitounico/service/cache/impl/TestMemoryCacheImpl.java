package br.com.bancointer.digitounico.service.cache.impl;

import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.service.cache.Cacheable;
import br.com.bancointer.digitounico.service.cache.MemoryCache;
import br.com.bancointer.digitounico.service.cache.annotations.Cached;
import br.com.bancointer.digitounico.service.cache.exception.CacheKeyNotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link MemoryCacheImpl}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class TestMemoryCacheImpl {

    @Autowired
    private MemoryCache myMemoryCache;

    @Mock
    private ProceedingJoinPoint mockedProceedingJoinPoint;

    @Mock
    private MethodSignature mockedMethodSignature;

    @Mock
    private Method mockedMethod;

    @Mock
    private Cached mockedCached;

    private UniqueNumber myUniqueNumber1;
    private UniqueNumber myUniqueNumber2;
    private UniqueNumber myUniqueNumber3;
    private UniqueNumber myUniqueNumber4;
    private UniqueNumber myUniqueNumber5;
    private UniqueNumber myUniqueNumber6;
    private UniqueNumber myUniqueNumber7;
    private UniqueNumber myUniqueNumber8;
    private UniqueNumber myUniqueNumber9;
    private UniqueNumber myUniqueNumber10;
    private UniqueNumber myUniqueNumber11;

    @BeforeEach
    public void setUp() {
        myUniqueNumber1 = buildUniqueNumber("1",1);
        myUniqueNumber2 = buildUniqueNumber("2",2);
        myUniqueNumber3 = buildUniqueNumber("3",3);
        myUniqueNumber4 = buildUniqueNumber("4",4);
        myUniqueNumber5 = buildUniqueNumber("5",5);
        myUniqueNumber6 = buildUniqueNumber("6",6);
        myUniqueNumber7 = buildUniqueNumber("7",7);
        myUniqueNumber8 = buildUniqueNumber("8",8);
        myUniqueNumber9 = buildUniqueNumber("9",9);
        myUniqueNumber10 = buildUniqueNumber("10",10);
        myUniqueNumber11 = buildUniqueNumber("11",11);

        when(mockedProceedingJoinPoint.getSignature()).thenReturn(mockedMethodSignature);
        when(mockedMethodSignature.getMethod()).thenReturn(mockedMethod);
        when(mockedMethod.getAnnotation(Cached.class)).thenReturn(mockedCached);
        when(mockedCached.value()).thenReturn(UniqueNumber.class);
    }

    @AfterEach
    public void afterEach() {
        myMemoryCache.clearCache();
    }

    @Test
    public void shouldProceedOnlyOnceForSameEntity() throws Throwable {
        prepareMocks(myUniqueNumber1);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber1);
        verify(mockedProceedingJoinPoint, times(1)).proceed(any());
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber1);
        verify(mockedProceedingJoinPoint, times(1)).proceed(any());
    }

    @Test
    public void shouldProceedTwiceForDifferentEntities() throws Throwable {
        prepareMocks(myUniqueNumber1);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber1);
        verify(mockedProceedingJoinPoint, times(1)).proceed(any());
        prepareMocks(myUniqueNumber2);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber2);
        verify(mockedProceedingJoinPoint, times(2)).proceed(any());
    }

    @Test
    public void shouldReplaceOldestEntry() throws Throwable {
        prepareMocks(myUniqueNumber1);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber1);
        verify(mockedProceedingJoinPoint, times(1)).proceed(any());
        prepareMocks(myUniqueNumber2);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber2);
        verify(mockedProceedingJoinPoint, times(2)).proceed(any());
        prepareMocks(myUniqueNumber3);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber3);
        verify(mockedProceedingJoinPoint, times(3)).proceed(any());
        prepareMocks(myUniqueNumber4);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber4);
        verify(mockedProceedingJoinPoint, times(4)).proceed(any());
        prepareMocks(myUniqueNumber5);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber5);
        verify(mockedProceedingJoinPoint, times(5)).proceed(any());
        prepareMocks(myUniqueNumber6);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber6);
        verify(mockedProceedingJoinPoint, times(6)).proceed(any());
        prepareMocks(myUniqueNumber7);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber7);
        verify(mockedProceedingJoinPoint, times(7)).proceed(any());
        prepareMocks(myUniqueNumber8);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber8);
        verify(mockedProceedingJoinPoint, times(8)).proceed(any());
        prepareMocks(myUniqueNumber9);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber9);
        verify(mockedProceedingJoinPoint, times(9)).proceed(any());
        prepareMocks(myUniqueNumber10);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber10);
        verify(mockedProceedingJoinPoint, times(10)).proceed(any());
        prepareMocks(myUniqueNumber1);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber1);
        verify(mockedProceedingJoinPoint, times(10)).proceed(any());
        prepareMocks(myUniqueNumber11);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber11);
        verify(mockedProceedingJoinPoint, times(11)).proceed(any());
        prepareMocks(myUniqueNumber2);
        assertEquals(myMemoryCache.searchInCache(mockedProceedingJoinPoint), myUniqueNumber2);
        verify(mockedProceedingJoinPoint, times(12)).proceed(any());
    }

    @Test
    public void shouldThrowIfThereIsNoMethodAnnotatedWithCacheKey() throws Throwable {
        when(mockedProceedingJoinPoint.getArgs()).thenReturn(new Object[]{""});
        when(mockedProceedingJoinPoint.proceed(any())).thenReturn(new TestCacheable());
        assertThrows(CacheKeyNotFoundException.class,() -> myMemoryCache.searchInCache(mockedProceedingJoinPoint));
    }

    private void prepareMocks(UniqueNumber uniqueNumber) throws Throwable {
        when(mockedProceedingJoinPoint.getArgs()).thenReturn(new Object[]{uniqueNumber.getNumber()});
        when(mockedProceedingJoinPoint.proceed(any())).thenReturn(uniqueNumber);
    }

    private UniqueNumber buildUniqueNumber(String number, Integer repeat) {
        return UniqueNumber.newBuilder()
                .withNumber(number)
                .withRepeat(repeat)
                .build();
    }

    private static class TestCacheable implements Cacheable<TestCacheable> {

        @Override
        public TestCacheable get() {
            return this;
        }
    }
}