package br.com.bancointer.digitounico.service.impl;

import br.com.bancointer.digitounico.dao.UniqueNumberRepository;
import br.com.bancointer.digitounico.model.UniqueNumber;
import br.com.bancointer.digitounico.service.UniqueNumberService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for {@link UniqueNumberServiceImpl}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TestUniqueNumberServiceImpl {

    @MockBean
    private UniqueNumberRepository myUniqueNumberRepository;

    @Autowired
    private UniqueNumberService myUniqueNumberService;

    @Test
    public void shouldComputeNumber10Once() {
        UniqueNumber uniqueNumber = myUniqueNumberService.computeUniqueNumber("10", "1");

        assertEquals(uniqueNumber.getResult(),1);
    }

    @Test
    public void shouldComputeNumber321Once() {
        UniqueNumber uniqueNumber = myUniqueNumberService.computeUniqueNumber("321", "1");

        assertEquals(uniqueNumber.getResult(),6);
    }

    @Test
    public void shouldComputeNumber9875FourTimes() {
        UniqueNumber uniqueNumber = myUniqueNumberService.computeUniqueNumber("9875", "4");

        assertEquals(uniqueNumber.getResult(),8);
    }

    @Test
    public void shouldComputeHugeNumberOnce() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int count = 0; count <= 1000000; count++) {
            stringBuilder.append("9");
        }

        UniqueNumber uniqueNumber = myUniqueNumberService.computeUniqueNumber(stringBuilder.toString(), "1");

        assertEquals(uniqueNumber.getResult(),9);
    }
}
