package br.com.bancointer.digitounico.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link UniqueNumber}
 */
public class TestUniqueNumber {
    private static final UUID SOME_UUID = UUID.randomUUID();
    private static final String SOME_NUMBER = "123";
    private static final int REPEAT_THREE_TIMES = 3;
    private static final int REPEAT_TWICE = 2;
    private static final int RESULT = 9;
    private static final int FIRST_ITERATION_RESULT = 18;

    @Test
    public void shouldCreateAnUniqueNumber() {
        UniqueNumber uniqueNumber = UniqueNumber.newBuilder()
                .withId(SOME_UUID)
                .withNumber(SOME_NUMBER)
                .withRepeat(REPEAT_THREE_TIMES)
                .withResult(RESULT)
                .withFirstIterationResult(FIRST_ITERATION_RESULT)
                .build();

        assertThat(uniqueNumber.getId()).isEqualTo(SOME_UUID);
        assertThat(uniqueNumber.getNumber()).isEqualTo(SOME_NUMBER);
        assertThat(uniqueNumber.getRepeat()).isEqualTo(REPEAT_THREE_TIMES);
        assertThat(uniqueNumber.getResult()).isEqualTo(RESULT);
        assertThat(uniqueNumber.getFirstIterationResult()).isEqualTo(FIRST_ITERATION_RESULT);
    }

    @Test
    public void shouldTestEqualsBehavior() {
        UniqueNumber uniqueNumber = UniqueNumber.newBuilder()
                .withId(SOME_UUID)
                .withNumber(SOME_NUMBER)
                .withRepeat(REPEAT_THREE_TIMES)
                .withResult(RESULT)
                .withFirstIterationResult(FIRST_ITERATION_RESULT)
                .build();

        UniqueNumber sameUniqueNumber = uniqueNumber.newBuilderFromCurrent().build();
        UniqueNumber differentRepeatUniqueNumber = uniqueNumber.newBuilderFromCurrent().withRepeat(REPEAT_TWICE).build();
        UniqueNumber nullUniqueNumber = null;

        assertThat(uniqueNumber.equals(uniqueNumber)).isTrue();
        assertThat(uniqueNumber.equals(sameUniqueNumber)).isTrue();
        assertThat(uniqueNumber.equals(differentRepeatUniqueNumber)).isFalse();
        assertThat(uniqueNumber.equals(nullUniqueNumber)).isFalse();
        assertThat(uniqueNumber.equals("UniqueNumber")).isFalse();
    }

    @Test
    public void shouldGenerateSameHashCodeForEqualUsers() {
        UniqueNumber uniqueNumber = UniqueNumber.newBuilder()
                .withId(SOME_UUID)
                .withNumber(SOME_NUMBER)
                .withRepeat(REPEAT_THREE_TIMES)
                .withResult(RESULT)
                .withFirstIterationResult(FIRST_ITERATION_RESULT)
                .build();

        UniqueNumber anotherUniqueNumber = UniqueNumber.newBuilder()
                .withId(SOME_UUID)
                .withNumber(SOME_NUMBER)
                .withRepeat(REPEAT_THREE_TIMES)
                .withResult(RESULT)
                .withFirstIterationResult(FIRST_ITERATION_RESULT)
                .build();

        assertThat(uniqueNumber.hashCode()).isEqualTo(anotherUniqueNumber.hashCode());
    }
}
