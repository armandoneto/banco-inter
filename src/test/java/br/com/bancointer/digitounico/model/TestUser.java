package br.com.bancointer.digitounico.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link User}
 */
public class TestUser {

    private static final UUID SOME_UUID = UUID.randomUUID();
    private static final String SOME_NAME = "Some Name";
    private static final String SOME_EMAIL = "some@mail.com";
    private static final String SOME_INVALID_EMAIL = "some*mail.com";
    private static final String ANOTHER_NAME = "Another name";

    @Test
    public void shouldCreateAnUser() {
        User user = User.newBuilder()
                .withId(SOME_UUID)
                .withName(SOME_NAME)
                .withEmail(SOME_EMAIL)
                .build();

        assertThat(user.getId()).isEqualTo(SOME_UUID);
        assertThat(user.getName()).isEqualTo(SOME_NAME);
        assertThat(user.getEmail()).isEqualTo(SOME_EMAIL);
    }

    @Test
    public void shouldTestEqualsBehavior() {
        User user = User.newBuilder()
                .withId(SOME_UUID)
                .withName(SOME_NAME)
                .withEmail(SOME_INVALID_EMAIL)
                .build();

        User sameUser = user.newBuilderFromCurrent().build();
        User differentNameUser = user.newBuilderFromCurrent().withName(ANOTHER_NAME).build();
        User nullUser = null;

        assertThat(user.equals(user)).isTrue();
        assertThat(user.equals(sameUser)).isTrue();
        assertThat(user.equals(differentNameUser)).isFalse();
        assertThat(user.equals(nullUser)).isFalse();
        assertThat(user.equals("User")).isFalse();
    }

    @Test
    public void shouldGenerateSameHashCodeForEqualUsers() {
        User user = User.newBuilder()
                .withId(SOME_UUID)
                .withName(SOME_NAME)
                .withEmail(SOME_INVALID_EMAIL)
                .build();

        User anotherUser = User.newBuilder()
                .withId(SOME_UUID)
                .withName(SOME_NAME)
                .withEmail(SOME_INVALID_EMAIL)
                .build();

        assertThat(user.hashCode()).isEqualTo(anotherUser.hashCode());
    }
}
