package br.com.bancointer.digitounico.rest.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for {@link UniqueNumberInputDTO}
 */
class TestUniqueNumberInputDTO {

    private static final String SOME_NUMBER = "123";
    private static final String REPEAT_THREE_TIMES = "3";
    private static final String ALL_ZEROES = "0000000";

    private Validator myValidator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        myValidator = factory.getValidator();
    }

    @Test
    public void shouldCreateAnUniqueNumberDTO() {
        UniqueNumberInputDTO uniqueNumberInputDTO = new UniqueNumberInputDTO();
        uniqueNumberInputDTO.setMyNumber(SOME_NUMBER);
        uniqueNumberInputDTO.setMyRepeat(REPEAT_THREE_TIMES);

        assertThat(uniqueNumberInputDTO.getMyNumber()).isEqualTo(SOME_NUMBER);
        assertThat(uniqueNumberInputDTO.getMyRepeat()).isEqualTo(REPEAT_THREE_TIMES);

        Set<ConstraintViolation<UniqueNumberInputDTO>> violations = myValidator.validate(uniqueNumberInputDTO);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void shouldThrowIfAllZeroesAreProvided() {
        UniqueNumberInputDTO uniqueNumberInputDTO = new UniqueNumberInputDTO();
        uniqueNumberInputDTO.setMyNumber(ALL_ZEROES);
        uniqueNumberInputDTO.setMyRepeat(REPEAT_THREE_TIMES);

        assertThat(uniqueNumberInputDTO.getMyNumber()).isEqualTo(ALL_ZEROES);
        assertThat(uniqueNumberInputDTO.getMyRepeat()).isEqualTo(REPEAT_THREE_TIMES);

        Set<ConstraintViolation<UniqueNumberInputDTO>> violations = myValidator.validate(uniqueNumberInputDTO);
        assertFalse(violations.isEmpty());
    }
}