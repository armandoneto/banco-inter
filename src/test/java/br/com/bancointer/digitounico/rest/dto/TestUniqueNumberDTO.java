package br.com.bancointer.digitounico.rest.dto;

import br.com.bancointer.digitounico.model.UniqueNumber;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link UniqueNumberDTO}
 */
class TestUniqueNumberDTO {

    private static final UUID SOME_UUID = UUID.randomUUID();
    private static final String SOME_NUMBER = "123";
    private static final int REPEAT_THREE_TIMES = 3;
    private static final int REPEAT_TWICE = 2;
    private static final int RESULT = 9;
    private static final int FIRST_ITERATION_RESULT = 18;

    @Test
    public void shouldCreateAnUniqueNumberDTO() {
        UniqueNumberDTO uniqueNumberDTO = new UniqueNumberDTO();
        uniqueNumberDTO.setId(SOME_UUID);
        uniqueNumberDTO.setNumber(SOME_NUMBER);
        uniqueNumberDTO.setRepeat(REPEAT_THREE_TIMES);
        uniqueNumberDTO.setResult(RESULT);
        uniqueNumberDTO.setFirstIterationResult(FIRST_ITERATION_RESULT);

        assertThat(uniqueNumberDTO.getId()).isEqualTo(SOME_UUID);
        assertThat(uniqueNumberDTO.getNumber()).isEqualTo(SOME_NUMBER);
        assertThat(uniqueNumberDTO.getRepeat()).isEqualTo(REPEAT_THREE_TIMES);
        assertThat(uniqueNumberDTO.getResult()).isEqualTo(RESULT);
        assertThat(uniqueNumberDTO.getFirstIterationResult()).isEqualTo(FIRST_ITERATION_RESULT);
    }

}